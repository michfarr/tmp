class Artist < Sequel::Model
  plugin :validation_helpers

  def validate
    super
    validates_presence [:name]
    validates_unique [:name]
  end
end

class Album < Sequel::Model
  plugin :validation_helpers

  def validate
    super
    validates_presence [:name, :artist_id]
  end
end

class Song < Sequel::Model
  plugin :validation_helpers

  def validate
    super
    validates_presence [:name, :artist_id, :album_id]
  end
end
