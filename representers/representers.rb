require 'roar/json/hal'

class ArtistRepresenter
  include Roar::JSON::HAL

  property :name
  
  link :self do
   "http://localhost:3000/artists/#{id}"
  end
end

class AlbumRepresenter
  include Roar::JSON::HAL

  property :title

  property :artist

  link :self do
    "http://localhost:3000/albums/#{id}"
  end
end

class SongRepresenter
  include Roar::JSON::HAL

  property :title

  property :artist
  property :album

  link :self do
    "http://localhost:3000/songs/#{id}"
  end
end
