require 'sinatra'
require 'json'
require './db/setup'
require './models/models'
require './representers/representers'

# Comment
class MyApp < Sinatra::Base
  get '/' do
    'Hello World'
  end

  get '/artists' do
    @artists = Artist.all
    @artists.to_json
  end

  get '/albums' do
    @albums = Album.all
    @albums.to_json
  end

  get '/songs' do
    @songs = Song.all
    @songs.to_json
  end
end
