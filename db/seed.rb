require 'sequel'

artists = DB[:artists]
albums = DB[:albums]
songs = DB[:songs]

tables = [:artists, :albums, :songs]

artist_names = %w('First', 'Second', 'Third')
names = %w('Spoon', 'Fork', 'Knife')
names = %w('Potato', 'Tomato', 'Basil', 'Sausage', 'Mandarijn')

def insert_seeds seed_array
  seed_array.each do |entry|
    artists.insert(entry)
  end
end

def generate_seeds name, random_number
  seeds = []

  random_number.times do |i|
    seeds << { name: "#{name} #{i}" }
  end

  insert_seeds seeds
end

def generate_seeds_array artist_name_array, max_seeds=10
  rand_total = rand(max_seeds)

  artist_name_array.each do |name|
    generate_seeds name, rand_total
  end
end

generate_seeds_array artist_names
