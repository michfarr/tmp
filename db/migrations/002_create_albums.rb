# Migration for Albums
class CreateAlbums < Sequel::Migration
  def up
    create_table? :albums do
      primary_key :id
      foreign_key :artists_id, :artists
      String      :name
    end
  end

  def down
    drop_table :albums
  end
end
