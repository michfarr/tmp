# Migration for Artists
class CreateArtists < Sequel::Migration
  def up
    create_table? :artists do
      primary_key :id
      String :name
    end
  end

  def down
    drop_table :albums, :artists
  end
end
