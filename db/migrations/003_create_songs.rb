# Migration for Songs
class CreateSongs < Sequel::Migration
  def up
    create_table? :songs do
      primary_key :id
      foreign_key :artists_id, :artists
      foreign_key :albums_id, :albums
      String      :name
    end
  end

  def down
    drop_table :songs, :albums, :artists
  end
end
