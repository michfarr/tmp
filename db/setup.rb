require 'sequel'
require 'logger'

DB ||= if ENV['RACK_ENV'] == 'test'
         # Sequel.connect(ENV['TEST_DATABASE_URL']) ||
         Sequel.connect('postgress://superman:clarkkent@localhost/blendle_app_test_db')
       else
         # Sequel.connect(ENV['DATABASE_URL']) ||
         Sequel.connect('postgres://superman:clarkkent@localhost/blendle_app_db')
       end

DB.logger = Logger.new($stdout)
