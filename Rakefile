Dir['tasks/**/*.rake'].each { |task| load task }

if %w(development test).include?(ENV['RACK_ENV'])
  require 'rubocop/rake_task'
  RuboCop::RakeTask.new

  require 'cucumber/rake/task'
  Cucumber::Rake::Task.new do |t|
    t.cucumber_opts = %w(--format progress)
  end

  task default: [:rubocop, :cucumber]
end

namespace :db do
  require './db/setup'
  require 'logger'

  desc 'Prints current schema version'
  DB.run 'CREATE SCHEMA IF NOT EXISTS musicpedia_schema'

  task :version do
    version = if DB.tables.include?(:musicpedia_schema__schema_info)
                DB[:schema_info].first[:version]
              end || 0
    puts "Schema Version: #{version}"
  end

  desc 'Run migrations'
  task :migrate, [:version] do |_, args|
    Sequel.extension :migration
    if args[:version]
      puts "Migrating to version #{args[:version]}"
      Sequel::Migrator.run(DB, 'db/migrations', target: args[:version].to_i, table: :musicpedia_schema__schema_info)
    else
      puts 'Migrating to latest'
      Sequel::Migrator.run(DB, 'db/migrations', table: :musicpedia_schema__schema_info)
    end
  end

  desc 'Rollback migrations. If version not specified it rollsback to the previous version'
  task :rollback, [:version] do |_, args|
    Sequel.extension :migration
    version = args[:version] || DB[:musicpedia_schema__schema_info].first[:version] - 1
    if version
      puts "Rolling back to version #{version}"
      Sequel::Migrator.run(DB, 'db/migrations', target: version.to_i, table: :musicpedia_schema__schema_info)
    end
  end
end

namespace :server do
  desc 'Starts the server'
  task :puma do
    start_puma_server = 'bundle exec puma -C config/puma.rb'
    system(start_puma_server)
  end
end
